require 'rails_helper'

describe ServicesController, type: :controller do

  render_views

  describe 'GET #show' do

    describe 'GET #index' do
      it 'returns all services' do
        service = create :service_with_tags

        get :index, params: {format: :json}

        expected_hash = {
          name: service.name,
          tags: service.tags,
          address: service.address,
          city: service.city,
          administrative_area: service.administrative_area,
          zip: service.zip,
          latitude: service.latitude,
          longitude: service.longitude,
          phone_number: service.phone_number,
          nearest_train_station: service.nearest_train_station,
          requirements: service.requirements,
          information: service.information
        }

        assert_response :success
        expect(response.body).to eq [expected_hash].to_json
      end
    end

    it 'should show service' do
      service = create :service_with_tags

      get :show, params: { id: service.id, format: :json }

      expected_hash = {
        name: service.name,
        tags: service.tags,
        address: service.address,
        city: service.city,
        administrative_area: service.administrative_area,
        zip: service.zip,
        latitude: service.latitude,
        longitude: service.longitude,
        phone_number: service.phone_number,
        nearest_train_station: service.nearest_train_station,
        requirements: service.requirements,
        information: service.information
      }

      assert_response :success
      expect(response.body).to eq expected_hash.to_json
    end
  end
end
