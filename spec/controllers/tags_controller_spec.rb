require 'rails_helper'

describe TagsController, type: :controller do

  render_views

  describe 'GET #index' do
    it 'assigns all tags as @tags' do
      tag1 = create :tag_with_services
      tag2 = create :tag_with_services

      get :index, params: {format: :json}

      expected_hash1 = {
          name: tag1.name,
          services: tag1.services
      }
      expected_hash2 = {
          name: tag2.name,
          services: tag2.services
      }

      assert_response :success
      expect(response.body).to eq [expected_hash1, expected_hash2].to_json
    end
  end

  describe 'GET #show' do
    it 'should show tag' do
      tag = create :tag_with_services

      get :show, params: {id: tag.id, format: :json}

      expected_hash = {
        name: tag.name,
        services: tag.services
      }

      assert_response :success
      expect(response.body).to eq expected_hash.to_json
    end
  end
end