FactoryGirl.define do
  factory :service do
    name 'MyService'
    address '123 Main St'
    city 'New York City'
    administrative_area 'Brooklyn'
    zip '10001'
    latitude 40.8217
    longitude -73.9276
    phone_number '2011234567'
    nearest_train_station 'A - 23rd Street'
    requirements 'ID'
    information 'Open 24 hrs'

    factory :service_with_tags do
      transient do
        tags_count 5
      end
      after(:create) do |service, evaluator|
        create_list(:tag, evaluator.tags_count, services: [service])
      end
    end
  end
end
