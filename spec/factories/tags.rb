FactoryGirl.define do
  factory :tag do
    name 'MyTag'

    factory :tag_with_services do
      transient do
        services_count 5
      end
      after(:create) do |tag, evaluator|
        create_list(:service, evaluator.services_count, tags: [tag])
      end
    end
  end
end
