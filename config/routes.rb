Rails.application.routes.draw do

  root to: redirect("admin/services")

  resources :passwords, controller: "clearance/passwords", only: [:create]
  resource :session, controller: "clearance/sessions", only: [:create]

  resources :users, controller: "clearance/users", only: [:create] do
    resource :password,
      controller: "clearance/passwords",
      only: [:create, :edit, :update]
  end

  get "/sign_in" => "clearance/sessions#new", as: "sign_in"
  delete "/sign_out" => "clearance/sessions#destroy", as: "sign_out"

  constraints Clearance::Constraints::SignedIn.new do
    root to: redirect("admin/services"), as: :signed_in_root
  end

  namespace :admin do
    resources :services
    resources :tags

    root to: redirect("admin/services")
  end

  namespace :api do
    namespace :v1 do
      resources :tags do
        member do
          get "services"
          get "subtags"
        end
      end

      resources :services do
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
