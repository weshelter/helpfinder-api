class AddReferencesToTags < ActiveRecord::Migration[5.0]
  def change
    add_reference :tags, :parent, foreign_key: {to_table: :tags}
  end
end
