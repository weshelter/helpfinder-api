class AddServiceProperties < ActiveRecord::Migration[5.0]
  def change
    change_table :services do |t|
      t.string :address, null: false
      t.string :city
      t.string :administrative_area, null: false
      t.string :zip, null: false
      t.decimal :latitude, null: false
      t.decimal :longitude, null: false
      t.string :phone_number_1
      t.string :phone_number_1_extension
      t.string :phone_number_2
      t.string :phone_number_2_extension
      t.string :nearest_train_station
      t.string :requirements
      t.string :information
      t.string :notes
      t.string :hours
      t.string :contact_name
      t.string :contact_email
      t.string :web_url
    end
  end
end
