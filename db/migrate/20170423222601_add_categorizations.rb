class AddCategorizations < ActiveRecord::Migration[5.0]
  def change
    rename_table :services_tags, :categorizations
  end
end
