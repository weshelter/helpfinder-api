class CreateServicesTags < ActiveRecord::Migration[5.0]
  def change
    create_table :services_tags do |t|
      t.references :service, foreign_key: true
      t.references :tag, foreign_key: true
    end
  end
end
