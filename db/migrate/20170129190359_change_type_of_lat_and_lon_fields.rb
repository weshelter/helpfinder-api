class ChangeTypeOfLatAndLonFields < ActiveRecord::Migration[5.0]
  def up
    change_column :services, :latitude, :float, null: true
    change_column :services, :longitude, :float, null: true
  end

  def down
    change_column :services, :latitude, :decimal, null: false
    change_column :services, :longitude, :decimal, null: false
  end
end
