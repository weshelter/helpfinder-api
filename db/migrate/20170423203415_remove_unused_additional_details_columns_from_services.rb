class RemoveUnusedAdditionalDetailsColumnsFromServices < ActiveRecord::Migration[5.0]
  def up
    rename_column :services, :phone_number_1, :phone_number
    remove_column :services, :phone_number_1_extension
    remove_column :services, :phone_number_2
    remove_column :services, :phone_number_2_extension
    remove_column :services, :notes
    remove_column :services, :hours
    remove_column :services, :contact_name
    remove_column :services, :contact_email
    remove_column :services, :web_url
  end

  def down
    rename_column :services, :phone_number, :phone_number_1
    change_table :services do |t|
      t.string :phone_number_1_extension
      t.string :phone_number_2
      t.string :phone_number_2_extension
      t.string :requirements
      t.string :information
      t.string :notes
      t.string :hours
      t.string :contact_name
      t.string :contact_email
      t.string :web_url
    end
  end
end
