require 'optparse'
require 'roo'
require 'sanitize'

def parse_args
  options = {}

  o = OptionParser.new

  o.banner = 'Usage: rake db:csv_import -- [-p PATH, --path PATH, -c CITY, --city CITY]'
  o.on('-p PATH', '--path PATH') do |path|
    options[:path] = path
  end
  o.on('-c CITY', '--city CITY') do |city|
    options[:city] = city
  end
  args = o.order!(ARGV) {}
  o.parse!(args)

  abort('Path is required') if options[:path].nil?
  abort('City is required') if options[:city].nil?

  options

end

def create_service(row, city)
  return if row['Address'].nil? || row['Name'].downcase == 'example'
  row.each do |key, value|
    row[key] = Sanitize.fragment(value)
  end
  service = Service.where(:address => row['Address']).first_or_initialize({
    name: row['Name'],
    address: row['Address'],
    city: city,
    administrative_area: row['Borough'],
    zip: row['Zip'],
    phone_number: row['Phone'],
    nearest_train_station: row['Nearest Train Station'],
    requirements: row['Requirements'],
    information: row['Information']
  })
  tags = row['Tags'].split(', ').map do |tag_name|
    Tag.find_or_initialize_by(name: tag_name)
  end
  service.tags = tags
  puts "saving #{row['Name']}"
  service.save()
end

def import_services(path, city)
  spreadsheet = Roo::Spreadsheet.open(path)
  spreadsheet.each_with_pagename do |name, sheet|
    puts name
    sheet.parse(header_search: sheet.row(1), clean: true).drop(1).each do |row|
      next if row.values[0].nil?
      create_service(row, city)
    end
  end
end

namespace :db do
  task :csv_import => :environment do
    import_services('./data.xlsx', 'New York City')
  end
end
