require "administrate/base_dashboard"

class ServiceDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    tags: Field::HasMany,
    id: Field::Number,
    name: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    address: Field::String,
    city: Field::String,
    administrative_area: Field::String,
    zip: Field::String,
    latitude: Field::Number.with_options(decimals: 2),
    longitude: Field::Number.with_options(decimals: 2),
    phone_number: Field::String,
    nearest_train_station: Field::String,
    requirements: Field::String,
    information: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :name,
    :address,
    :tags,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :created_at,
    :updated_at,
    :address,
    :city,
    :administrative_area,
    :zip,
    :latitude,
    :longitude,
    :phone_number,
    :nearest_train_station,
    :requirements,
    :information,
    :tags,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :address,
    :city,
    :administrative_area,
    :zip,
    :phone_number,
    :nearest_train_station,
    :requirements,
    :information,
    :tags,
  ].freeze

  # Overwrite this method to customize how services are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(service)
    "Service - #{service.name}"
  end
end
