class Api::V1::ServicesController < ApplicationController
  # GET /services?lat=0.0&lng=0.0
  def index
    lat = params[:lat].to_f
    lng = params[:lng].to_f
    @non_geocoded_services = Service.not_geocoded
    @geocoded_services = Service.near([lat, lng], 30)
    render json: (@non_geocoded_services + @geocoded_services), lat: lat, lng: lng
  end

  # GET /services/:id
  def show
    @service = Service.find(params[:id])
    render json: @service
  end
end
