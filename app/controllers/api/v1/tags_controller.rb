class Api::V1::TagsController < ApplicationController
  # GET /api/v1/tags
  def index
    @tags = Tag.where("parent_id IS ?", nil)
    render json: @tags
  end

  # GET /api/v1/tags/:id
  def show
    @tag = Tag.find(params[:id])
    render json: @tag
  end

  # GET /api/v1/tags/:id/subtags
  def subtags
    parent = params[:id]
    @tags = Tag.where(parent_id: parent)
    render json: @tags
  end

  # GET /api/v1/tags/:id/services?lat=0.0&lng=0.0
  def services
    @tag = Tag.find(params[:id])
    lat = params[:lat].to_f
    lng = params[:lng].to_f
    @non_geocoded_services = @tag.services.not_geocoded
    @geocoded_services = @tag.services.near([lat, lng], 30)
    render json: (@non_geocoded_services + @geocoded_services), lat: lat, lng: lng
  end
end
