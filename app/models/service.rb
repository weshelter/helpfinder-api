class Service < ApplicationRecord
  has_many :categorizations, :dependent => :destroy
  has_many :tags, :through => :categorizations

  def full_street_address
    [address, administrative_area, city, zip].compact.join(', ')
  end

  geocoded_by :full_street_address

  after_validation :geocode
end
