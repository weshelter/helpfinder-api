class Tag < ApplicationRecord
  has_many :categorizations, :dependent => :destroy
  has_many :services, :through => :categorizations
  belongs_to :parent, class_name: "Tag", optional: true
end
