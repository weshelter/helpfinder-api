class TagSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :services
  belongs_to :parent, class_name: "Tag"
end
