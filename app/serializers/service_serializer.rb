class ServiceSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :city, :administrative_area,
             :zip, :latitude, :longitude, :phone_number,
             :nearest_train_station, :requirements, :information, :distance

  def distance
    object.respond_to?(:distance) ? object.distance : nil
  end
  has_many :tags
end
